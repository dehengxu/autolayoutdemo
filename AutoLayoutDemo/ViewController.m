//
//  ViewController.m
//  AutoLayoutDemo
//
//  Created by NicholasXu on 14-2-11.
//  Copyright (c) 2014年 Deheng.Xu. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Layout.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.view.backgroundColor = [UIColor lightGrayColor];
    self.title = @"AutoLayout";
    
    if (!_left) self.left = [UIView new];
    if (!_right) self.right = [UIView new];
    if (!_center) self.center = [UIView new];
    
    [self.view addSubview:self.left];
    [self.view addSubview:self.right];
    [self.view addSubview:self.center];
    
    self.view.translatesAutoresizingMaskIntoConstraints = self.left.translatesAutoresizingMaskIntoConstraints = self.right.translatesAutoresizingMaskIntoConstraints = self.center.translatesAutoresizingMaskIntoConstraints = NO;

    self.left.backgroundColor = [UIColor redColor];
    self.right.backgroundColor = [UIColor greenColor];
    self.center.backgroundColor = [UIColor blueColor];

//#if !TEST_AUTO_LAYOUT
    /*
      center 放置在顶部
      left 放置在 center 的下部的左边区域
      right 放置在 center 的下部右边区域
      以上三个视图与父视图的边距为 32 points，他们三者之间的间距也为 32 points。
     */
    
    __block NSDictionary *views = NSDictionaryOfVariableBindings(_left, _right, _center);
    
    __block ViewController *weakSelf = self;



    [self.view addMyLayoutContraints:^(NSMutableArray<NSLayoutConstraint *> * __nonnull constraints) {
        
        //left & right
        
        // 设置left 和 right等宽 等高
        [constraints addObject:[NSLayoutConstraint constraintWithItem:weakSelf.left attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:weakSelf.right attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
        
        [constraints addObject:[NSLayoutConstraint constraintWithItem:weakSelf.left attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:weakSelf.right attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
        
        // 设置 left ,right view 等间距为 32
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"|-32-[_left]-32-[_right]-32-|" options:0 metrics:nil views:views]];
        
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_left]-32-|" options:0 metrics:nil views:views]];
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_right]-32-|" options:0 metrics:nil views:views]];
        
        // 设置 center 在顶部
        // center 和 left 间距 32
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-32-[_center(==120)]-32-[_left]" options:0 metrics:nil views:views]];
        // center 左右边距 32
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"|-32-[_center]-32-|" options:0 metrics:nil views:views]];

    }];
    
    [self.view addLayoutConstraints:^(NSMutableArray *const constraints) {
        //left & right
        
//        // 设置left 和 right等宽 等高
//        [constraints addObject:[NSLayoutConstraint constraintWithItem:weakSelf.left attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:weakSelf.right attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
//        
//        [constraints addObject:[NSLayoutConstraint constraintWithItem:weakSelf.left attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:weakSelf.right attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
//        
//        // 设置 left ,right view 等间距为 32
//        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"|-32-[_left]-32-[_right]-32-|" options:0 metrics:nil views:views]];
//        
//        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_left]-32-|" options:0 metrics:nil views:views]];
//        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_right]-32-|" options:0 metrics:nil views:views]];
//        
//        // 设置 center 在顶部
//        // center 和 left 间距 32
//        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-32-[_center(==120)]-32-[_left]" options:0 metrics:nil views:views]];
//        // center 左右边距 32
//        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"|-32-[_center]-32-|" options:0 metrics:nil views:views]];

    }];

    NSLog(@"constraints :%@", self.view.constraints);

    
//#endif
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"center :%@", NSStringFromCGRect(self.center.frame));
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)shouldAutorotate
//{
//    return YES;
//}

@end
