//
//  ViewController.h
//  AutoLayoutDemo
//
//  Created by NicholasXu on 14-2-11.
//  Copyright (c) 2014年 Deheng.Xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *left, *center, *right;

@end
