//
//  UIView+Layout.h
//  AutoLayoutDemo
//
//  Created by DehengXu on 15/6/24.
//  Copyright © 2015年 Deheng.Xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Layout)

- (void)addLayoutConstraints:(__nullable void (^)(__nullable NSMutableArray * const constraints)) fillConstraintsBlock;

@end
