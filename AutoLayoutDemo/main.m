
//
//  main.m
//  AutoLayoutDemo
//
//  Created by NicholasXu on 14-2-11.
//  Copyright (c) 2014年 Deheng.Xu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
