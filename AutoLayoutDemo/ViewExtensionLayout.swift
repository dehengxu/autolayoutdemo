//
//  ViewExtensionLayout.swift
//  AutoLayoutDemo
//
//  Created by DehengXu on 15/6/24.
//  Copyright © 2015年 Deheng.Xu. All rights reserved.
//

import Foundation
import UIKit

@objc class MyView: UIView  {
    
}

extension UIView {

    func addMyLayoutContraints(makeLayoutConstraints: (NSMutableArray)->())
    {
        let cons: NSMutableArray = NSMutableArray()
        
        makeLayoutConstraints(cons)
        
        self.addConstraints((cons as NSArray as! [NSLayoutConstraint]));
    }
    
}