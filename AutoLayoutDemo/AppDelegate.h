//
//  AppDelegate.h
//  AutoLayoutDemo
//
//  Created by NicholasXu on 14-2-11.
//  Copyright (c) 2014年 Deheng.Xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
