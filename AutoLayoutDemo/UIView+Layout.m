//
//  UIView+Layout.m
//  AutoLayoutDemo
//
//  Created by DehengXu on 15/6/24.
//  Copyright © 2015年 Deheng.Xu. All rights reserved.
//

#import "UIView+Layout.h"

@implementation UIView (Layout)

- (void)addLayoutConstraints:(void (^)(NSMutableArray * const constraints))fillConstraintsBlock
{
    if (fillConstraintsBlock == NULL) return;
    
    NSMutableArray *mutableLayouts = [@[] mutableCopy];
    
    //Let user fill 'mutableLayouts' with layout constraints.
    fillConstraintsBlock(mutableLayouts);
    
    if (nil == mutableLayouts || mutableLayouts.count == 0) return;
    
    [self addConstraints:mutableLayouts];
}

@end
